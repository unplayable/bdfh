﻿using Microsoft.Extensions.Configuration;

namespace GameServer.Repository
{
    public sealed class MongoRepository
    {
        private IConfiguration Configuration { get; }

        public MongoRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}