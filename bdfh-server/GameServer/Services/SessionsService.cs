﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading.Tasks;
using GameServer.Services.Sessions;
using Microsoft.AspNetCore.Http;
using Utils;

namespace GameServer.Services
{
    public sealed class SessionsService
    {
        private readonly Dictionary<string, Session> _userSessions = new();

        private async Task CreateSession(string connectionId, WebSocket websocket)
        {
            Console.WriteLine($"[{connectionId}] attempt to create session.");
            if (_userSessions.ContainsKey(connectionId))
            {
                
                Console.WriteLine($"[{connectionId}] session already exists.");
                return;
            }

            var session = new Session(connectionId, websocket);

            Console.WriteLine($"[{connectionId}] getting token ...");
            var (success, tokenString) = await session.GetToken();
            if (!success)
            {
                Console.WriteLine($"[{connectionId}] Getting token failed.");
                session.Close();
                return;
            }

            Console.WriteLine($"[{connectionId}] Getting token succeed => {tokenString}.");
            var token = new Token(tokenString);
            if (!Sign.ValidateSign(token.Sign, token.UserId))
            {
                session.SendDebug($"[{connectionId}] Token validation failed: Invalid Sign.");
                Console.WriteLine($"[{connectionId}] Token validation failed: Invalid Sign.");
                session.Close();
                return;
            }

            if (token.Expiration < DateTimeOffset.UtcNow)
            {
                session.SendDebug($"[{connectionId}] Token validation failed: Expired.");
                Console.WriteLine($"[{connectionId}] Token validation failed: Expired.");
                session.Close();
                return;
            }

            Console.WriteLine($"[{connectionId}] Token validation succeed.");
            session.SendDebug($"Logged in {token.UserId}");
            _userSessions.Add(connectionId, session);
            _userSessions[connectionId].Closed += OnSessionClosed;
            await _userSessions[connectionId].Receive();
        }

        private void OnSessionClosed(string connectionId)
        {
            Console.WriteLine($"[{connectionId}] Closed.");
            _userSessions.Remove(connectionId);
        }
        
        public async Task CreateSession(HttpContext context)
        {
            if (context.Request.Path == "/game")
            {
                // TODO [Dmitrii Osipov]: authenticate token from auth server
                if (!context.WebSockets.IsWebSocketRequest)
                    context.Response.StatusCode = 400;
                else
                {
                    using var websocket = await context.WebSockets.AcceptWebSocketAsync();

                    await CreateSession(context.Connection.Id, websocket);
                    await context.Response.WriteAsync("OK!");
                }
            }
        }
    }
}