﻿using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// ReSharper disable ConstantConditionalAccessQualifier

namespace GameServer.Services.Sessions
{
    public sealed class Session
    {
        private const float SocketTimeOut = 30f;
        private const int SendBufferSize = 4096;
        private const int ReceiveBufferSize = 4096;

        private const int MessageBufferSize = 65536;

        private readonly string _connectionId;
        private readonly CancellationTokenSource _cts;
        private readonly ArraySegment<byte> _buffer;
        private readonly byte[] _messageBuffer = new byte[MessageBufferSize];

        private readonly WebSocket _webSocket;

        public bool IsOpened => _webSocket is {State: WebSocketState.Open};

        public event Action<string> Closed;
        public event Action<string, string> Error;
        public event Action<string, byte[]> Received;

        public Session(string connectionId, WebSocket webSocket)
        {
            _connectionId = connectionId;
            _cts = new CancellationTokenSource();
            _webSocket = webSocket;
            _buffer = WebSocket.CreateClientBuffer(ReceiveBufferSize, SendBufferSize);
        }

        public void Send(byte[] data)
        {
            if (!_webSocket.SendAsync(new ArraySegment<byte>(data), WebSocketMessageType.Binary, true, CancellationToken.None)
                .Wait(TimeSpan.FromSeconds(SocketTimeOut)))
            {
                if(!string.IsNullOrEmpty(_webSocket.CloseStatusDescription))
                    Error?.Invoke(_connectionId, "Websocket timed out");
                Closed?.Invoke(_connectionId);
            }

            if (_webSocket.State == WebSocketState.Open) return;
            
            if(!string.IsNullOrEmpty(_webSocket.CloseStatusDescription))
                Error?.Invoke(_connectionId, _webSocket.CloseStatusDescription);
            Closed?.Invoke(_connectionId);
        }

        [Conditional("DEBUG")]
        internal void SendDebug(string text)
        {
            if (!_webSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(text)), WebSocketMessageType.Text, true, CancellationToken.None)
                .Wait(TimeSpan.FromSeconds(SocketTimeOut)))
            {
                
                if(!string.IsNullOrEmpty(_webSocket.CloseStatusDescription))
                    Error?.Invoke(_connectionId, "Websocket timed out");
                Closed?.Invoke(_connectionId);
            }

            if (_webSocket.State != WebSocketState.Open)
            {
                if(!string.IsNullOrEmpty(_webSocket.CloseStatusDescription))
                    Error?.Invoke(_connectionId, _webSocket.CloseStatusDescription);
                Closed?.Invoke(_connectionId);
            }
        }

        public void Close()
        {
            _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, _cts.Token);
            Closed?.Invoke(_connectionId);
        }

        internal async Task<(bool, string)> GetToken()
        {
            var count = 0;
            WebSocketReceiveResult receiveResult;
            do
            {
                receiveResult = await _webSocket.ReceiveAsync(_buffer, CancellationToken.None);
                if (receiveResult.CloseStatus.HasValue) return (false, string.Empty);

                Array.Copy(_buffer.Array, _buffer.Offset, _messageBuffer, count, receiveResult.Count);
                count += receiveResult.Count;
            } while (!receiveResult.EndOfMessage);

            if (count == 0) return (false, string.Empty);
            return (true, Encoding.UTF8.GetString(_messageBuffer, 0, count));
        }
        
        internal async Task Receive()
        {
            while (IsOpened)
            {
                var count = 0;
                WebSocketReceiveResult receiveResult;
                do
                {
                    receiveResult = await _webSocket.ReceiveAsync(_buffer, CancellationToken.None);
                    if (receiveResult.CloseStatus.HasValue)
                    {
                        if(!string.IsNullOrEmpty(_webSocket.CloseStatusDescription))
                            Error?.Invoke(_connectionId, _webSocket.CloseStatusDescription);
                        Closed?.Invoke(_connectionId);
                        return;
                    }

                    Array.Copy(_buffer.Array, _buffer.Offset, _messageBuffer, count, receiveResult.Count);
                    count += receiveResult.Count;
                } while (!receiveResult.EndOfMessage);

                if (count == 0) continue;

                var result = new byte[count];
                Array.Copy(_messageBuffer, result, count);
                Received?.Invoke(_connectionId, result);
            }
        }
    }
}