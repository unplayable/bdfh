﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Utils
{
    public static class Security
    {
        public static string Md5Hash(byte[] input)
        {
            var md5Hasher = MD5.Create();
            var data = md5Hasher.ComputeHash(input);
            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++) sBuilder.Append(data[i].ToString("x2"));

            return sBuilder.ToString();
        }

        public static string Md5Hash(string input) => Md5Hash(Encoding.UTF8.GetBytes(input));

        public static string SHA1Hash(byte[] bytes)
        {
            using var sha1 = new SHA1CryptoServiceProvider();
            return Convert.ToBase64String(sha1.ComputeHash(bytes));
        }
    }
}