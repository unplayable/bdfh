﻿using System;
using System.IO;
using System.Text;

namespace Utils
{
    public class Token
    {
        public long UserId { get; init; }
        public string Sign { get; init; }
        public DateTimeOffset Expiration { get; init; }
        
        public Token(long userId, string sign, TimeSpan expires)
        {
            UserId = userId;
            Sign = sign;
            Expiration = DateTimeOffset.UtcNow + expires;
        }

        public override string ToString()
        {
            using var ms = new MemoryStream();
            
            ms.Write(BitConverter.GetBytes(UserId));
            ms.Write(BitConverter.GetBytes(Sign.Length));
            ms.Write(Encoding.UTF8.GetBytes(Sign));
            ms.Write(BitConverter.GetBytes(Expiration.UtcTicks));
            
            return Convert.ToBase64String(ms.ToArray());
        }

        public Token(string token)
        {
            var data = Convert.FromBase64String(token);

            var current = 0;
            UserId = BitConverter.ToInt64(data, current);
            current += sizeof(ulong);
            var count = BitConverter.ToInt32(data, current);
            current += sizeof(int);
            Sign = Encoding.UTF8.GetString(data, current, count);
            current += Sign.Length;
            Expiration = new DateTimeOffset(BitConverter.ToInt64(data, current), TimeSpan.Zero);
        }
    }
}