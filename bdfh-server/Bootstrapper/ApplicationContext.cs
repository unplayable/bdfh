﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bootstrapper.Hosts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.Json;

namespace Bootstrapper
{
    internal sealed class ApplicationContext
    {
        public string[] Args { get; }
        
        private IConfigurationRoot Configuration { get; set; }
        
        private IReadOnlyDictionary<string, AssemblyContext> Services { get; set; }

        private IServiceHost KestrelHost { get; set; }
        private IServiceHost ConsoleHost { get; set; }
        
        private bool UseKestrel { get; set; }
        private bool UseWorkers { get; set; }
        
        public ApplicationContext(string[] args) => Args = args;

        public ApplicationContext ReadConfiguration()
        {
            Configuration = new ConfigurationBuilder()
                .Add(new CommandLineConfigurationSource{Args = Args})
                .Add(new JsonConfigurationSource{Path = "config.json"})
                .Build();
            
            return this;
        }

        public ApplicationContext ConfigureServices()
        {
            var services = new Dictionary<string, AssemblyContext>();
            var section = Configuration.GetSection("Bootstrapper:Services");
            
            foreach (var configurationSection in section.GetChildren())
            {
                var ctx = new AssemblyContext(configurationSection);
                if (ctx.IsKestrelService) UseKestrel = true;
                if (ctx.IsConsoleService) UseWorkers = true;
                services.Add(ctx.Name, ctx);
            }

            Services = services;
            return this;
        }

        public void RunServices()
        {
            if (UseKestrel) KestrelHost = new KestrelHost(this);
            if (UseWorkers) ConsoleHost = new ConsoleHost(this);

            foreach (var service in Services.Values)
            {
                if (service.IsKestrelService) KestrelHost.AddService(service);
                if (service.IsConsoleService) ConsoleHost.AddService(service);
            }
            
            var tasks = new List<Task>();
            if (UseKestrel) tasks.Add(KestrelHost.Run());
            if (UseWorkers) tasks.Add(ConsoleHost.Run());
            Task.WaitAll(tasks.ToArray());
        }
    }
}