﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Bootstrapper.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bootstrapper
{
    internal sealed class AssemblyContext : IDisposable
    {
        public IConfigurationSection Configuration { get; }
        private ServiceConfiguration ServiceConfiguration { get; }
        

        public string Name => ServiceConfiguration.Name;
        public bool IsKestrelService => ServiceConfiguration.IsKestrelService;
        public bool IsConsoleService => ServiceConfiguration.IsConsoleService;
        public string RootDirectory => ServiceConfiguration.Root;
        public string AssemblyName => ServiceConfiguration.AssemblyName;

        private Assembly _assembly;
        private MethodInfo _configure;
        private MethodInfo _configureServices;

        private object _startup;

        public AssemblyContext(IConfigurationSection configuration)
        {
            Configuration = configuration;
            ServiceConfiguration = Configuration.Get<ServiceConfiguration>();

            var alc = new AssemblyLoadContext(ServiceConfiguration.Name);
            var assemblyPath = new FileInfo(Path.Combine(RootDirectory, $"{AssemblyName}.dll")).FullName;

            _assembly = alc.LoadFromAssemblyPath(assemblyPath);
            foreach (var referenceName in _assembly.GetReferencedAssemblies())
            {
                try
                {
                    assemblyPath = new FileInfo(Path.Combine(RootDirectory, $"{referenceName.Name}.dll")).FullName;
                    if (File.Exists(assemblyPath)) alc.LoadFromAssemblyPath(assemblyPath);
                    else alc.LoadFromAssemblyName(referenceName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            var startupType = _assembly.DefinedTypes.FirstOrDefault(t => t.Name.EndsWith("Startup"));
            if (startupType != null)
            {
                _startup = Activator.CreateInstance(startupType);
                
                _configureServices = startupType.GetMethod(nameof(ConfigureServices));
                _configure = startupType.GetMethod(nameof(Configure));
            }
        }
        public void Dispose()
        {
            _assembly = null;
            _startup = null;
            _configureServices = null;
            _configure = null;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            _configureServices?.Invoke(_startup, new object[] {Configuration, services});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IEndpointRouteBuilder routeBuilder)
        {
            _configure?.Invoke(_startup, new object[] {app, env, routeBuilder});
        }
    }
}