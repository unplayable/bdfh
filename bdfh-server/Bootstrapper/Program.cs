namespace Bootstrapper
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new ApplicationContext(args)
                .ReadConfiguration()
                .ConfigureServices()
                .RunServices();
        }
    }
}