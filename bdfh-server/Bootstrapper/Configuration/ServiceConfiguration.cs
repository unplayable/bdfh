﻿namespace Bootstrapper.Configuration
{
    internal sealed class ServiceConfiguration
    {
        public string Name { get; init; }
        public string Root { get; init; }
        public string AssemblyName { get; init; }
        public bool IsKestrelService { get; init; }
        public bool IsConsoleService { get; init; }
    }
}