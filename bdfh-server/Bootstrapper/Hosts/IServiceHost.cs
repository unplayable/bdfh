﻿using System.Threading.Tasks;

namespace Bootstrapper.Hosts
{
    internal interface IServiceHost
    {
        Task Run();
        void AddService(AssemblyContext service);
    }
}