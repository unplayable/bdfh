﻿using System.Threading.Tasks;

namespace Bootstrapper.Hosts
{
    internal sealed class ConsoleHost : IServiceHost
    {
        public ConsoleHost(ApplicationContext ctx)
        {
        }

        public async Task Run()
        {
            await Task.CompletedTask;
        }

        public void AddService(AssemblyContext service)
        {
            
        }
    }
}