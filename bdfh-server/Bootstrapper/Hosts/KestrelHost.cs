﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Bootstrapper.Hosts
{
    internal sealed class KestrelHost : IServiceHost
    {
        public KestrelHost(ApplicationContext ctx) => Context = ctx;
        private ApplicationContext Context { get; }

        private List<AssemblyContext> Services { get; } = new();

        public IHost Host { get; private set; }

        public async Task Run()
        {
            Host = Microsoft.Extensions.Hosting.Host
                .CreateDefaultBuilder(Context.Args)
                .ConfigureWebHostDefaults(whb => whb.UseStartup(_ => new AggregatedStartup(Services)))
                .Build();

            await Host.RunAsync();
        }

        public void AddService(AssemblyContext service)
        {
            Services.Add(service);
        }

        private sealed class AggregatedStartup
        {
            private readonly IReadOnlyList<AssemblyContext> _services;
            public AggregatedStartup(List<AssemblyContext> services) => _services = services;

            public void ConfigureServices(IServiceCollection services)
            {
                foreach (var startup in _services)
                    startup.ConfigureServices(services);
            }

            // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
            {
                if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

                app.UseRouting();

                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapGet("/status", async context => { await context.Response.WriteAsync("Some web status!"); });
                    
                    foreach (var startup in _services)
                        startup.Configure(app, env, endpoints);
                });

            }
        }
    }
}