﻿namespace AuthServer
{
    public enum Platform
    {
        // ReSharper disable InconsistentNaming
        Undefined = 0,
        VK,
        Dev
        // ReSharper restore InconsistentNaming
    }
    
    public sealed class UserAuth
    {
        public Platform Platform { get; init; }
        public string Token { get; init; }
    }
}