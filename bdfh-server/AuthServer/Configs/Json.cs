﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace AuthServer.Configs
{
    public enum JsonFormatting : byte
    {
        None,
        Intended
    }

    public static class Json
    {
        public static string Serialize(object value, JsonFormatting formatting) => JsonSerializer.Serialize(value, formatting == JsonFormatting.None ? DefaultOptions : IntendedOptions);
        public static T Deserialize<T>(string json) => JsonSerializer.Deserialize<T>(json, DefaultOptions)!;

        public static JsonSerializerOptions DefaultOptions { get; } = new(JsonSerializerDefaults.Web)
        {
            AllowTrailingCommas = true,
            Converters =
            {
                new JsonStringEnumConverter(),
            }
        };
        
        public static JsonSerializerOptions IntendedOptions { get; } = new(DefaultOptions)
        {
            WriteIndented = true
        };
    }
}