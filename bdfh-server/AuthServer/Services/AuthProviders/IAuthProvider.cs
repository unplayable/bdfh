﻿using System.Threading.Tasks;

namespace AuthServer.Services.AuthProviders
{
    internal interface IAuthProvider
    {
        Task<(bool, string)> Authorize(UserAuth userAuth);
    }
}

