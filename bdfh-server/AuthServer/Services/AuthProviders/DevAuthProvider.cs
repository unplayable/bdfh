﻿using System.Threading.Tasks;

namespace AuthServer.Services.AuthProviders
{
    internal sealed class DevAuthProvider : IAuthProvider
    {
        public async Task<(bool, string)> Authorize(UserAuth userAuth)
        {
            // TODO [Dmitriy Osipov] token is platform id for now
            return (true, userAuth.Token);
        }
    }
}