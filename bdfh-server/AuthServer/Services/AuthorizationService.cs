﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AuthServer.Configs;
using AuthServer.Repository;
using AuthServer.Services.AuthProviders;
using Microsoft.AspNetCore.Http;
using Utils;

namespace AuthServer.Services
{
    internal sealed class AuthorizationService
    {
        private MongoRepository Repository { get; }
        
        private readonly Dictionary<Platform, IAuthProvider> _authProviders;
        
        public AuthorizationService(MongoRepository repository)
        {
            Repository = repository;
            _authProviders = new Dictionary<Platform, IAuthProvider>
            {
                {Platform.Dev, new DevAuthProvider()},
                {Platform.VK, new VkAuthProvider()}
            };
        }

        public async Task Authorize(HttpContext context)
        {
            if (context.Request.Method != HttpMethods.Post)
            {
                context.Response.StatusCode = 403;
                return;
            }

            UserAuth auth;
            try
            {
                auth = await ReadFromBody<UserAuth>(context);
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 403;
                return;
            }
            
            if (!_authProviders.TryGetValue(auth.Platform, out var provider))
            {
                context.Response.StatusCode = 403;
                return;
            }
            
            var (success, platformId) = await provider.Authorize(auth);
            if(!success)
            {
                context.Response.StatusCode = 403;
                return;
            }

            var userIdentity = await Repository.GetOrCreateUserAsync(auth.Platform, platformId);

            await context.Response.WriteAsync(new Token(userIdentity.InternalId, userIdentity.Sign, TimeSpan.FromMinutes(5))
                .ToString());
        }

        private async Task<T> ReadFromBody<T>(HttpContext context)
        {
            using var sr = new StreamReader(context.Request.Body);
            return Json.Deserialize<T>(await sr.ReadToEndAsync());
        }
    }
}