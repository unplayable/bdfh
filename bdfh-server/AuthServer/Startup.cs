using AuthServer.Repository;
using AuthServer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AuthServer
{
    public class Startup
    {
        public void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddSingleton(new MongoRepository(configuration));
            services.AddSingleton<AuthorizationService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IEndpointRouteBuilder routeBuilder)
        {
            // ReSharper disable once PossibleNullReferenceException
            routeBuilder.MapPost("/auth", app.ApplicationServices.GetService<AuthorizationService>().Authorize);
        }
    }
}