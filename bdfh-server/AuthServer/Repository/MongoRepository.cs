﻿using System;
using System.Threading.Tasks;
using AuthServer.Repository.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace AuthServer.Repository
{
    public sealed class MongoRepository
    {
        private static readonly FilterDefinitionBuilder<UserIdentity> UserIdentityFilter = Builders<UserIdentity>.Filter;
        private static readonly UpdateDefinitionBuilder<UserIdentity> UserIdentityUpdate = Builders<UserIdentity>.Update;
        
        private static readonly FilterDefinitionBuilder<AuthSettings> AuthSettingsFilter = Builders<AuthSettings>.Filter;
        private static readonly UpdateDefinitionBuilder<AuthSettings> AuthSettingsUpdate = Builders<AuthSettings>.Update;

        private readonly DatabaseConfig _config;
        
        private readonly MongoClient _client;
        private readonly IClientSessionHandle _session;
        private readonly IMongoDatabase _database;
        
        private IConfiguration Configuration { get; }

        private IMongoCollection<UserIdentity> UserIdentities(Platform platform) => _database.GetCollection<UserIdentity>($"users_{platform.ToString()}");

        private async Task<long> GetNextUserId()
        {
            var result = await _database.GetCollection<AuthSettings>("settings")
            .FindOneAndUpdateAsync(
                AuthSettingsFilter.Empty,
                AuthSettingsUpdate.Inc(authSettings => authSettings.CurrentUserId, 1),
                new FindOneAndUpdateOptions<AuthSettings>
                {
                    IsUpsert = true,
                    ReturnDocument = ReturnDocument.After
                });

            if (result != null) return result.CurrentUserId;
            
            result = new AuthSettings
            {
                CurrentUserId = _config.StartUid
            };
            await _database.GetCollection<AuthSettings>("settings").InsertOneAsync(result);

            return result.CurrentUserId;
        }
        
        public MongoRepository(IConfiguration configuration)
        {
            Configuration = configuration;

            var repositorySection = Configuration.GetSection("Repository");
            // var mongoUrl = new MongoUrl(["ConnectionString"]);
            // var identity = new MongoInternalIdentity(mongoUrl.DatabaseName, mongoUrl.Username);
            // var evidence = new PasswordEvidence(mongoUrl.Password);
            // var credential = new MongoCredential("SCRAM-SHA-1", identity, evidence);
            // var settings = new MongoClientSettings
            // {
            //     Credential = credential,
            //     Servers = mongoUrl.Servers
            // };
            
            var settings =
                MongoClientSettings.FromConnectionString(repositorySection["ConnectionString"]);
            _client = new MongoClient(settings);

            _session = _client.StartSession();
            _database = _client.GetDatabase("auth");

            _config = repositorySection.GetSection("Config").Get<DatabaseConfig>();
        }

        public async Task<UserIdentity> GetOrCreateUserAsync(Platform platform, string platformId)
        {
            var userIdentity = await GetUser(platform, platformId);
            if (userIdentity != null) return userIdentity;

            return await CreateUser(platform, platformId);
        }

        public async Task<UserIdentity> GetUser(Platform platform, string platformId)
        {
            var user = await UserIdentities(platform).FindOneAndUpdateAsync(
                UserIdentityFilter.Eq(ui => ui.PlatformId, platformId),
                UserIdentityUpdate.Set(ui => ui.LastLogin, DateTimeOffset.UtcNow));
            return user;
        }

        public async Task<UserIdentity> CreateUser(Platform platform, string platformId)
        {
            var internalId = await GetNextUserId();
            var userIdentity = new UserIdentity
            {
                Platform = platform,
                PlatformId = platformId,
                InternalId = internalId,
                Created = DateTimeOffset.UtcNow,
                LastLogin = DateTimeOffset.UtcNow
            };
            
            await UserIdentities(platform).InsertOneAsync(userIdentity);
            return userIdentity;
        }
    }

    internal sealed class DatabaseConfig
    {
        public long StartUid { get; init; }
    }
}