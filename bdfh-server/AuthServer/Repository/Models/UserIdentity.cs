﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AuthServer.Repository.Models
{
    public sealed class UserIdentity
    {
        [BsonId] public ObjectId _id;
        [BsonRequired] public long InternalId { get; init; }
        [BsonRequired] public Platform Platform { get; init; }
        [BsonRequired] public string PlatformId { get; init; }
        [BsonRequired] public string Sign { get; init; }
        [BsonRequired] public DateTimeOffset Created { get; init; }
        [BsonRequired] public DateTimeOffset LastLogin { get; init; }
    }
}