﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AuthServer.Repository.Models
{
    public sealed class AuthSettings
    {
        [BsonId] public ObjectId _id;
        public long CurrentUserId { get; set; }
    }
}