function _isIncluded([string[]] $excludes)
{
    $itemPath = $_.FullName
    if (IsDirectory($itemPath))
    {
        return $false
    }

    foreach ($exclude in $excludes)
    {
        if ($itemPath -like $exclude)
        {
            return $false;
        }
    }

    return $true;
}

function IsFileLocked {
    param (
      [parameter(Mandatory=$true)][string]$Path
    )
  
    $oFile = New-Object System.IO.FileInfo $Path
  
    if ((Test-Path -Path $Path) -eq $false) {
      return $false
    }
  
    try {
      $oStream = $oFile.Open([System.IO.FileMode]::Open, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None)
  
      if ($oStream) {
        $oStream.Close()
      }
      return $false
    } catch {
      # file is locked by a process.
      return $true
    }
  }

<#
.DESCRIPTION
IsDirectory validates that specified path is existed directory
#>
function IsDirectory([string] $path)
{
    (Test-Path $path) -and ((Get-Item $path) -is [System.IO.DirectoryInfo])
}

<#
.DESCRIPTION
CheckSubmoduleDirectory validates that submodule directory exists. Otherwise execution will be terminated
#>
function CheckSubmoduleDirectory([string] $path)
{
    $name = (Split-Path $path -Leaf);
    Info ('> ' + $name)

    if (IsDirectory $path)
    {
        Trace ('Path: ' + $path)
    }
    else
    {
        Error ('Submodule was not found: ' + $path)
    }

    $name
}

function GetBranchName
{
    [OutputType([string])]
    param (
        # Repository root directory
        [Parameter(Mandatory)]
        [string]
        $repositoryPath
    )

    $branchName = StartProcess 'git' 'rev-parse --abbrev-ref HEAD' $repositoryPath -ScalarOut
    return $branchName
}

<#
.DESCRIPTION
CheckSubmoduleBranch validates that submodule on master branch.
#>
function CheckSubmoduleBranch([string] $submodulePath, [bool] $forceReset)
{
    $submoduleName = CheckSubmoduleDirectory($submodulePath)
    $branchName = GetBranchName $submodulePath

    if ($branchName -ne 'master')
    {
        if ($forceReset)
        {
            StartProcess 'git' 'checkout -f -B master remotes/origin/master --' $submodulePath
        }
        else
        {
            Error(('Submodule "' + $submoduleName + '" is not on master branch. Run with "force reset" submodule or "skip git" mod'))
        }
    }
    else
    {
        StartProcess 'git' 'pull' $submodulePath
    }
}

function CopyFile
{
    param
    (
        [parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [string] $filePath,
        [parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [string] $to,
        [switch] $Silent = $false,
        [switch] $NoTrace = $false
    )

    if (Test-Path $filePath)
    {
        if (IsDirectory $filePath)
        {
            Error ('File is expected: ' + $filePath)
        }

        if (!(Test-Path $to))
        {
            if ([System.IO.Path]::HasExtension($to) -or ([System.IO.Path]::GetFileName($to) -eq [System.IO.Path]::GetFileName($filePath)))
            {
                $targetDirPath = Split-Path -Path $to -Parent
            }
            else
            {
                $targetDirPath = $to
            }

            CheckDir $targetDirPath
        }

        if (!$NoTrace)
        {
            Trace ('Copy from "' + $filePath + '" to "' + $to + '"')    
        }

        Copy-Item $filePath $to
    }
    elseif (!$Silent)
    {
        Error ('File was not found: "' + $filePath + '"')
    }
}

function GetFilesPath {
    param (
        [parameter(Mandatory)]
        [string] $source,
        [string[]] $excludes
    )
    
    
    if (!(IsDirectory $source))
    {
        Error ('Directory is expected "{0}"' -f $source) 
    }

    $files = Get-ChildItem $source -Recurse | Where-Object -FilterScript ( { _isIncluded($excludes) } ) | ForEach-Object { $_.FullName }
    return $files
}

<#
.DESCRIPTION
RoboCopy copy files from source to destination based on excludes
#>
function RoboCopy
{
    param
    (
        [parameter(Mandatory)]
        [string] $source,
        [parameter(Mandatory)]
        [string] $destination,
        [string[]] $excludes,
        [switch] $copySelf = $false
    )

    if (!(IsDirectory $source))
    {
        Error ('Directory is expected "{0}"' -f $source) 
    }

    if ($copySelf)
    {
        $destination = Join-Path $destination (Split-Path $source -Leaf)
    }

    CheckDir $destination
    Trace ('Copy From: "' + $source + '" To: "' + $destination + '"')

    $files = GetFilesPath $source $excludes
    foreach ($filePath in $files) 
    {
        CopyFile $filePath (Join-Path $destination $filePath.Substring($source.length)) -Silent -NoTrace 
    }
}

<#
.DESCRIPTION
RoboRemove removes files from source
#>
function RoboRemove
{
    param
    (
        [parameter(Mandatory)]
        [string] $path
    )

    if (Test-Path $path)
    {
        Remove-Item $path -Force -Recurse
    }
}

<#
.DESCRIPTION
CheckDir creates directory if not exist
#>
function CheckDir
{
    param
    (
        [parameter(Mandatory)]
        [string] $path
    )

    if (!(Test-Path $path))
    {
        New-Item -ItemType Directory -Force -Path $path | out-null
    }
}

function JoinArrayPath
{
    param
    (
        [parameter(Mandatory)]
        [string[]]$PathElements
    ) 

    if ($PathElements.Length -eq "0")
    {
        $CombinedPath = ""
    }
    else
    {
        $CombinedPath = $PathElements[0]
        for ($i = 1; $i -lt $PathElements.Length; $i++)
        {
            $CombinedPath = Join-Path $CombinedPath $PathElements[$i]
        }
    }
    return $CombinedPath
}

function StartProcess
{
    param (
        [Parameter(Mandatory)]
        [string]$FilePath,
        [string]$Arguments,
        [string]$WorkingDirectory,
        [switch]$ScalarOut = $false
    )

    Trace ('Runing new process: "' + $FilePath + '" ' + $Arguments)

    if (!$WorkingDirectory)
    {
        $WorkingDirectory = Get-Location
    }

    $processInfo = New-Object System.Diagnostics.ProcessStartInfo
    $processInfo.FileName = $FilePath
    $processInfo.WorkingDirectory = $WorkingDirectory
    $processInfo.RedirectStandardError = $true
    $processInfo.RedirectStandardOutput = $true
    $processInfo.UseShellExecute = $false

    if ($Arguments) 
    {
        $processInfo.Arguments = $Arguments
    }

    $process = New-Object System.Diagnostics.Process
    $process.StartInfo = $processInfo
    $process.Start() | Out-Null
    $stdout = $process.StandardOutput.ReadToEnd()
    $stderr = $process.StandardError.ReadToEnd()

    $process.WaitForExit()

    if ($stdout)
    {
        Trace $stdout
    }

    if ($process.ExitCode -ne 0)
    {
        $message = ('Process "' + $FilePath + '" is exited with error: ' + $process.ExitCode)

        if ($stderr)
        {
            $message += ' '
            $message += $stderr
        }

        Error $message $process.ExitCode
    }
    else
    {
        Trace 'Process succefully completed.'
        $LASTEXITCODE = 0
    }

    if ($stdout)
    {
        $result = $stdout -split '\r?\n'
        if ($result.Count -gt 0 -and $ScalarOut) 
        {
            for ($i = $result.Count - 1; $i -ge 0 ; $i--)
            {
                $line = $result[$i]
                if ($line)
                {
                    return $line
                }
            }

            return $null
        }

        return $result
    }
}

function GetHumanBool
{
    param
    (
        [parameter(Mandatory)]
        [bool] $condition
    )

    if ($condition)
    { 
        return "Yes"
    }
    else
    {
        return "No" 
    }
}

function DotNetRestore
{
    param
    (
        [Parameter(Mandatory)]
        [string] $projectFullPath,
        [string] $options
    )

    $argvs = 'restore -v m ' + $projectFullPath + ' --no-dependencies'
    if ($options)
    {
        $argvs += (' ' + $options)
    }

    Trace ('dotnet ' + $argvs)
    StartProcess 'dotnet' $argvs
}

function DotNetBuild
{
    param
    (
        [Parameter(Mandatory)]    
        [string] $projectFullPath,
        [Parameter(Mandatory)]
        [string] $config,
        [Parameter(Mandatory)]
        [string] $targetFramework,
        [string] $outputDir,
        [string] $options
    )

    $argvs = 'build ' + $projectFullPath + ' -c ' + $config + ' -f ' + $targetFramework
    if ($outputDir)
    {
        $argvs += (' -o ' + $outputDir)
    }

    if ($options)
    {
        $argvs += (' ' + $options)
    }

    Trace ('dotnet ' + $argvs)
    StartProcess 'dotnet' $argvs
}

function DotNetRun {
    param
    (
        [Parameter(Mandatory)]
        [string] $projectFullPath,
        [Parameter(Mandatory)]
        [string] $argvs
    )

    Trace ('dotnet ' + $projectFullPath + ' -v m ' + $argvs)
    StartProcess 'dotnet ' ($projectFullPath + ' -v m ' + $argvs)
}

function DotNetTest
{
    param
    (
        [Parameter(Mandatory)]
        [string] $projectFullPath,
        [Parameter(Mandatory)]
        [string] $config
    )

    Trace ('dotnet test ' + $projectFullPath + ' -c ' + $config)
    StartProcess 'dotnet' ('test ' + $projectFullPath + ' -c ' + $config)
}

function DotNetPublish
{
    param
    (
        [Parameter(Mandatory)]
        [string] $projectFullPath,
        [Parameter(Mandatory)]
        [string] $config,
        [Parameter(Mandatory)]
        [string] $targetFramework,
        [Parameter(Mandatory)]
        [string] $publishDir
    )
    Trace ('dotnet publish ' + $projectFullPath + ' -c ' + $config + ' -f ' + $targetFramework + ' -o ' + $publishDir)
    StartProcess 'dotnet' ('publish ' + $projectFullPath + ' -c ' + $config + ' -f ' + $targetFramework + ' -o ' + $publishDir)
}

function Info
{
    param
    (
        [parameter(Mandatory)]
        [string] $message
    )

    Write-Host $message -BackgroundColor Black -ForegroundColor Green
}

function Warn
{
    param
    (
        [parameter(Mandatory)]
        [string] $message
    )

    Write-Host $message -BackgroundColor Black -ForegroundColor Yellow
}

function LogEmptyLine 
{
    Write-Host ' '
}

function Trace
{
    param
    (
        [parameter(Mandatory)]
        [string] $message
    )

    Write-Host $message -BackgroundColor Black -ForegroundColor Gray
}

function Error
{
    param
    (
        [parameter(Mandatory)]
        [string] $message,
        [int] $exitCode = 502
    )

    Write-Host $message -BackgroundColor Black -ForegroundColor Red
    throw "Unexpected termination of the build process. Please contact your Administrator."
    #[System.Environment]::Exit(-1); this will close the shell
}

Export-ModuleMember -Function @(
    'Error',
    'Trace',
    'Info',
    'Warn',
    'LogEmptyLine',
    'StartProcess',
    'JoinArrayPath',
    'CheckDir',
    'RoboRemove',
    'RoboCopy',
    'GetFilesPath',
    'CopyFile',
    'GetBranchName',
    'CheckSubmoduleBranch',
    'CheckSubmoduleDirectory',
    'IsDirectory',
    'GetHumanBool',
    'DotNetBuild',
    'DotNetTest',
    'DotNetRun',
    'DotNetPublish',
    'DotNetRestore',
    'IsFileLocked'
)

Export-ModuleMember -Variable @('IsBuildUtilsImported')