class ModuleConfig
{
    [string]$Name
    [string]$Project
    [string]$Output
}

function AddModule {
    param
    (
        [Parameter(Mandatory)]
        $DirectoryName,
        
        [Parameter(Mandatory)]
        $IntermeidateDir
    )

    $module = [ModuleConfig]::new()
    $module.Name = $DirectoryName
    $module.Project = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($projectPath, $DirectoryName, $DirectoryName +".csproj"))
    
    if($DirectoryName -eq "Bootstrapper"){
        $module.Output = [System.IO.Path]::GetFullPath($IntermeidateDir)
    }
    else {
        $module.Output = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($IntermeidateDir, $DirectoryName))
    }

    $modules.Add($module)
}
