[CmdletBinding()]
param
(
    [Alias('p', 'project-dir')] [string]$projectPath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, '..')),
    [Alias('b', 'build-dir')] [string]$buildScriptsPath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot)),
    [Alias('-c', '-configuration')] [ValidateSet('Debug','Release')][Parameter(Mandatory=$true, Position=1)] $configuration,
    [Alias('-a', '-auth')] [Switch] $auth=$false,
    [Alias('-g', '-game')] [Switch] $gameserver=$false
    # add other services here
)

$LASTEXITCODE = 0
Import-Module (Join-Path $buildScriptsPath 'utils.psm1')
. (Join-Path $buildScriptsPath 'usings.ps1')

Info ('Beginning this madness...')

$intermeidateDir = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($projectPath, '.intermediate'))
Trace ('-> ' + $intermeidateDir)

CheckDir $intermeidateDir

#Warn ('Included projects:')
$modules = New-Object System.Collections.ArrayList

AddModule 'Bootstrapper' $intermeidateDir
if($auth) { AddModule 'AuthServer' $intermeidateDir }
if($gameserver) { AddModule 'GameServer' $intermeidateDir }

Info ('-> DotNet Restore begin...')

foreach($module in $modules) {
    Info ('---> DotNet Restore [' + $module.Name + '] begin ...')
    DotNetRestore $module.Project
    Info ('---> DotNet Restore [' + $module.Name + '] completed.')
}

Info ('-> DotNet Restore completed.')
Info ('-> DotNet Publish begin...')

foreach($module in $modules) {
    Info ('---> DotNet Restore [' + $module.Name + '] begin ...')
    DotNetPublish $module.Project $configuration 'net5.0' $module.Output
    Info ('---> DotNet Restore [' + $module.Name + '] completed.')
}

Info ('-> DotNet Publish completed.')
Info ('-> Packing build...')

$publishDir = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($projectPath, '..', '.publish/server'))
Trace ('-> ' + $publishDir)

Remove-Item $publishDir -Recurse -Force
CheckDir $publishDir

RoboCopy $intermeidateDir $publishDir

Info ('-> Packing build completed.')
Info ('Madness is completed!')