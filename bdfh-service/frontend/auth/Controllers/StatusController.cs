namespace auth.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class StatusController : ControllerBase
    {
        [HttpGet]
        public StatusInfo Get()
        {
            return new StatusInfo("OK");
        }
    }

    public class StatusInfo
    {
        public string State { get; init; }
        public long   Time  { get; init; }

        public StatusInfo(string state)
        {
            State = state;
            Time  = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }
    }
}