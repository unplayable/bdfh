namespace gateway.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class GatewayController : ControllerBase
    {
        [HttpPost(Name = "Auth")]
        public void Auth(LoginRequest request)
        {
            
        }
        
        [HttpPost(Name = "Login")]
        public void Login(LoginRequest request)
        {
            
        }
    }

    public class AuthRequest
    {
    }
    
    public class LoginRequest
    {
    }
}