﻿using MessagePack;

namespace Game.API
{
    [MessagePackObject]
    public struct LoginRequest
    {
        [Key(0)] public string LoginId;
    }

    [MessagePackObject]
    public struct LoginResponse
    {
        [Key(1)] public string UserId;
    }
}