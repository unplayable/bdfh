# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /app

RUN apt-get update && apt-get install tree

# Copy everything else and build
COPY bdfh-common/API/ bdfh-common/API/
COPY bdfh-server/GameServer/ bdfh-server/GameServer/

RUN tree bdfh-server/
RUN tree bdfh-common/

# restore projects
RUN dotnet restore ./bdfh-common/API/API.csproj
RUN dotnet restore ./bdfh-server/GameServer/GameServer.csproj

RUN tree bdfh-server/
RUN tree bdfh-common/

# publish dotnet app
RUN dotnet publish bdfh-server/GameServer/GameServer.csproj -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /app
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "GameServer.dll"]
